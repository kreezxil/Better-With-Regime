[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

Genetic Animals has a post mod that is made from both the vertically and horizontally aligned vanilla slabs. This mod adds recipes to convert the post to the Silent's Mechanisms' recipe for drying racks which by default uses horizontally alligned slabs but doesn't get loaded because Genetical Animals sorts higher in the priority.

Can you spot that bad recipes?

Spoiler (click to show)

![](https://i.imgur.com/OeQCMuS.png)

![](https://i.imgur.com/XZxDJeV.png)

![](https://i.imgur.com/DS3IUBk.png)

![](https://i.imgur.com/0S2GPLE.png)

![](https://i.imgur.com/20ACJOx.png)

![](https://i.imgur.com/dhQJFTF.png)

The recipes depicted below convert the Genetical Animals' post to the Silent Mechanisms drying rack.

![](https://i.imgur.com/5DiwfNh.png)

![](https://i.imgur.com/SPUAltx.png)

![](https://i.imgur.com/ep5V7UD.png)

![](https://i.imgur.com/LwimPN0.png)

![](https://i.imgur.com/v52JS5U.png)

![](https://i.imgur.com/rnndNXt.png)
